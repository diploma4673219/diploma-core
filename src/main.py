import sys
import os
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.staticfiles import StaticFiles

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from src.clients.frontend.routes import router as frontend_routes
from src.core.exceptions import exception_handlers
from src.settings import origins, MEDIA_ROOT, settings

os.makedirs(MEDIA_ROOT, exist_ok=True)


def create_app() -> FastAPI:
    _app = FastAPI(exception_handlers=exception_handlers)

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )

    _app.include_router(frontend_routes)
    _app.mount("/media", StaticFiles(directory=MEDIA_ROOT), name="media")

    return _app


app = create_app()


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.HOST,
        port=settings.PORT,
        reload=True
    )
