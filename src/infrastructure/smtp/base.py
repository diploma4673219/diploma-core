from fastapi import HTTPException
from jinja2 import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from aiosmtplib import SMTP
from src.settings import settings


EMAIL_SUBJECT_CONFIRMATION = "Подтверждение регистрации"
EMAIL_SUBJECT_SCORECARD = "Ведомость по курсу"


def generate_confirmation_email(token: str):
    template = Template("""
    <html>
        <body>
            <h2>Привет!</h2>
            <p>Спасибо за регистрацию. Пожалуйста, подтвердите свой адрес электронной почты, нажав на ссылку ниже:</p>
            <a href="{{ url }}/auth/confirm?token={{ token }}">Подтвердите почту</a>
        </body>
    </html>
    """)
    return template.render(url=settings.FRONTEND_URL, token=token)


def generate_scorecard_email(link: str, course_title: str):
    template = Template("""
        <html>
            <body>
                <h2>Привет!</h2>
                <p>Ведомость по курсу <b>{{ course_title }}</b> готова</p>
                <a href="{{ link }}">Скачать</a>
            </body>
        </html>
        """)
    return template.render(link=link, course_title=course_title)


def generate_assistant_finish_email(block_title: str, full_name: str, email: str):
    template = Template("""
        <html>
            <body>
                <h2>Привет!</h2>
                <p><b>{{ full_name }}</b> проверил все работы по блоку <b>{{ block_title }}</b></p>
                <p>Почта ассистента: <a href="mailto:{{ email }}">{{ email }}</a></p>
            </body>
        </html>
        """)
    return template.render(email=email, full_name=full_name, block_title=block_title)


def generate_block_finish_email(block_title: str):
    template = Template("""
        <html>
            <body>
                <h2>Привет!</h2>
                <p>Все работы по блоку <b>{{ block_title }}</b> проверены</p>
            </body>
        </html>
        """)
    return template.render(block_title=block_title)


async def send_email(recipient: str, body: str, subject: str):
    msg = MIMEMultipart()
    msg['From'] = settings.SMTP_USERNAME
    msg['To'] = recipient
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'html'))

    try:
        async with SMTP(hostname=settings.SMTP_SERVER, port=settings.SMTP_PORT) as server:
            await server.login(settings.SMTP_USERNAME, settings.SMTP_PASSWORD)
            await server.send_message(msg)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Ошибка при отправке письма: {e}")
