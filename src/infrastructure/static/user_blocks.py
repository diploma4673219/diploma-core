import os
import pandas as pd

from src.core.domain.blocks.entities import ScorecardColumns
from src.settings import MEDIA_ROOT, settings


def get_csv_from_data(data, file_name):
    # Создаем DataFrame из исходных данных.
    df = pd.DataFrame(data)

    # Добавляем колонку 'original_index' для сохранения изначального порядка строк.
    df[ScorecardColumns.ORDER] = range(len(df))

    # Создаем сводную таблицу (pivot_table).
    pivot_df = df.pivot_table(index=[ScorecardColumns.FULL_NAME, ScorecardColumns.GROUP],
                              columns=ScorecardColumns.BLOCK, values=ScorecardColumns.SCORE, aggfunc='first')

    # Сбрасываем индекс.
    pivot_df = pivot_df.reset_index()

    # Добавляем колонку 'Команда'.
    pivot_df[ScorecardColumns.TEAM] = pivot_df.apply(
        lambda row: df[(df[ScorecardColumns.FULL_NAME] == row[ScorecardColumns.FULL_NAME])
                       & (df[ScorecardColumns.GROUP] == row[ScorecardColumns.GROUP])][ScorecardColumns.TEAM]
        .dropna().iloc[0],
        axis=1
    )

    # Восстанавливаем оригинальный индекс для сортировки.
    pivot_df[ScorecardColumns.ORDER] = pivot_df.apply(
        lambda row: df[
            (df[ScorecardColumns.FULL_NAME] == row[ScorecardColumns.FULL_NAME]) &
            (df[ScorecardColumns.GROUP] == row[ScorecardColumns.GROUP])
            ][ScorecardColumns.ORDER]
        .dropna().iloc[0],
        axis=1
    )

    # Переформатируем таблицу для вывода.
    pivot_df = pivot_df[[ScorecardColumns.FULL_NAME, ScorecardColumns.GROUP, ScorecardColumns.TEAM]
                        + [col for col in pivot_df.columns if
                           col not in [ScorecardColumns.FULL_NAME, ScorecardColumns.GROUP, ScorecardColumns.TEAM]]]

    # Сортируем по оригинальному индексу и убираем колонку original_index перед сохранением.
    pivot_df = pivot_df.sort_values(by=[ScorecardColumns.ORDER]).drop(columns=[ScorecardColumns.ORDER])

    # Определение полного пути к файлу и сохранение в CSV.
    file_full_path = os.path.join(MEDIA_ROOT, file_name)
    pivot_df.to_csv(file_full_path, index=False, sep=';')

    result = f'{settings.EXTERNAL_HOST}/media/{file_name}'
    return result
