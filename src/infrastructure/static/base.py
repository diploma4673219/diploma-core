import os
from src.settings import MEDIA_ROOT


def save_file(file_name, file_content):
    file_full_path = os.path.join(MEDIA_ROOT, file_name)

    with open(file_full_path, mode="wb") as f:
        f.write(file_content)

    return file_name
