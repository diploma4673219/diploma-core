from typing import List

from sqlalchemy import select, desc, or_
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import aliased

from src.infrastructure.database.db import get_db_session
from fastapi import Depends

from src.core.domain.blocks.entities import BlocksDTO, UserBlocksListDTO, BlocksListDTO, UserBlocksDTO, BlockListFilterArgs, UserBlocksListParams
from ..models import User
from ..models.blocks import Block, UserBlock
from src.infrastructure.database.repositories.base import CRUDRepo
from ..models.teams import Team


class BlocksRepo(CRUDRepo):
    model = Block
    dto_model = BlocksDTO

    async def list(self, args: BlockListFilterArgs) -> BlocksListDTO:
        query = select(self.model)

        # Apply filters.
        filter_args = self._prepare_where_args_for_list(args)
        query = query.where(*filter_args)

        # Apply ordering.
        query = query.order_by(desc(self.model.order))

        result = await self.session.execute(query)
        blocks = result.scalars().all()

        return [self.dto_model.from_orm(block) for block in blocks]

    def _prepare_where_args_for_list(self, args: BlockListFilterArgs):
        result = [self.model.course_id == args.course_id]

        if args.status is not None:
            result.append(self.model.status.in_(args.status))

        return result


def get_blocks_repository(db_session: AsyncSession = Depends(get_db_session)) -> BlocksRepo:
    return BlocksRepo(db_session)


class UserBlocksRepo(CRUDRepo):
    model = UserBlock
    dto_model = UserBlocksDTO
    related_fields = ['user', 'assistant', 'block', 'team']
    user_alias = aliased(User, name='user')
    assistant_alias = aliased(User, name='assistant')

    async def list(self, args: UserBlocksListParams) -> UserBlocksListDTO:
        query = select(self.model).\
            join(Block, self.model.block_id == Block.id).\
            join(self.user_alias, self.model.user_id == self.user_alias.id).\
            outerjoin(self.assistant_alias, self.model.assistant_id == self.assistant_alias.id).\
            outerjoin(Team, self.model.team_id == Team.id)

        # Apply filters.
        filter_args = self._prepare_where_args_for_list(args)
        query = query.where(*filter_args)

        # Apply ordering.
        order_args = self._prepare_order_args_for_list(args)
        query = query.order_by(*order_args)

        # Apply padding.
        total_count = await self._get_total_count_for_query(query)
        offset = (args.page - 1) * args.page_size
        query = query.limit(args.page_size).offset(offset)

        result = await self.session.execute(query)
        user_blocks = result.scalars().all()

        result = []
        for user_block in user_blocks:
            block_full_data = await self.get_by_id(user_block.id)
            result.append(block_full_data)

        return UserBlocksListDTO(result, total_count)

    def _prepare_where_args_for_list(self, args: UserBlocksListParams):
        result = [Block.course_id == args.course_id]

        if args.block_id is not None:
            result.append(self.model.block_id == args.block_id)
        if args.assistant_id is not None:
            result.append(self.model.assistant_id == args.assistant_id)
        if args.score is not None:
            result.append(self.model.score == args.score)
        if args.is_author is not None:
            result.append(self.model.is_author == args.is_author)
        if args.is_team is not None:
            result.append(Block.is_team == args.is_team)
        if args.search is not None:
            search_expr = or_(
                self.user_alias.last_name.ilike(f"%{args.search}%"),
                self.user_alias.group.ilike(f"%{args.search}%"),
                self.user_alias.email.ilike(f"%{args.search}%"),
                self.assistant_alias.last_name.ilike(f"%{args.search}%"),
                Block.title.ilike(f"%{args.search}%"),
                Team.name.ilike(f"%{args.search}%"),
            )
            result.append(search_expr)

        return result

    def _prepare_order_args_for_list(self, args: UserBlocksListParams):
        SORTING_MAPPING = {
            "last_name": self.user_alias.last_name,
            "group": self.user_alias.group,
            "assistant": self.assistant_alias.last_name,
            "block": Block.order,
            "score": self.model.score,
            "team": Team.name
        }

        sort = []
        if args.sort and args.sort.strip("-") in SORTING_MAPPING:
            desc_flag = args.sort.startswith("-")
            sorting_key = args.sort.strip("-")
            sorting_column = SORTING_MAPPING[sorting_key]
            sort = [desc(sorting_column) if desc_flag else sorting_column]

        sort.append(self.model.id)
        return sort


def get_user_blocks_repository(db_session: AsyncSession = Depends(get_db_session)) -> UserBlocksRepo:
    return UserBlocksRepo(db_session)
