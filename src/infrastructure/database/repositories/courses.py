from typing import List

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.infrastructure.database.db import get_db_session
from fastapi import Depends

from src.core.domain.courses.entities import CoursesDTO, UserCoursesDTO, CoursesListDTO, UserCoursesListDTO
from ..models.courses import Course, UserCourse
from src.infrastructure.database.repositories.base import CRUDRepo


class CoursesRepo(CRUDRepo):
    model = Course
    dto_model = CoursesDTO

    async def list(self, ids: List[int]) -> CoursesListDTO:
        query = select(self.model).filter(self.model.id.in_(ids))
        result = await self.session.execute(query)
        courses = result.scalars().all()

        return [self.dto_model.from_orm(course) for course in courses]


def get_courses_repository(db_session: AsyncSession = Depends(get_db_session)) -> CoursesRepo:
    return CoursesRepo(db_session)


class UserCoursesRepo(CRUDRepo):
    model = UserCourse
    dto_model = UserCoursesDTO
    related_fields = ['user', 'course']

    async def list(self, **kwargs) -> UserCoursesListDTO:
        query = select(self.model).filter_by(**kwargs)

        result = await self.session.execute(query)
        user_courses = result.scalars().all()

        result = []
        for user_course in user_courses:
            course_full_data = await self.get_by_id(user_course.id)
            result.append(course_full_data)
        return result


def get_user_courses_repository(db_session: AsyncSession = Depends(get_db_session)) -> UserCoursesRepo:
    return UserCoursesRepo(db_session)
