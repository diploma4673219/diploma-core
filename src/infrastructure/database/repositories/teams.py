from sqlalchemy import select, or_
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from src.infrastructure.database.db import get_db_session
from src.infrastructure.database.repositories.base import CRUDRepo
from fastapi import Depends

from src.core.domain.teams.entities import TeamsDTO, UserTeamsDTO, TeamsListDTO, UserTeamsListDTO, UserTeamsListParams
from ..models.teams import Team, UserTeam
from ..models.users import User


class TeamsRepo(CRUDRepo):
    model = Team
    dto_model = TeamsDTO

    def _load_related_fields(self, select_stm):
        select_stm = select_stm.options(selectinload(self.model.user_teams).selectinload(UserTeam.user))
        return select_stm

    async def list(self, **kwargs) -> TeamsListDTO:
        query = select(self.model).filter_by(**kwargs)
        result = await self.session.execute(query)
        teams = result.scalars().all()

        return [self.dto_model.from_orm(team) for team in teams]


def get_teams_repository(db_session: AsyncSession = Depends(get_db_session)) -> TeamsRepo:
    return TeamsRepo(db_session)


class UserTeamsRepo(CRUDRepo):
    model = UserTeam
    dto_model = UserTeamsDTO
    related_fields = ['user']

    async def list(self, args: UserTeamsListParams) -> UserTeamsListDTO:
        query = select(self.model).join(Team, self.model.team_id == Team.id).join(User, self.model.user_id == User.id)

        # Apply filters.
        filter_args = self._prepare_where_args_for_list(args)
        query = query.where(*filter_args)

        result = await self.session.execute(query)
        user_teams = result.scalars().all()

        result = []
        for user_team in user_teams:
            team_full_data = await self.get_by_id(user_team.id)
            result.append(team_full_data)
        return result

    def _prepare_where_args_for_list(self, args: UserTeamsListParams):
        result = [Team.course_id == args.course_id]

        if args.team_id:
            result.append(self.model.team_id == args.team_id)
        if args.user_id:
            result.append(self.model.user_id == args.user_id)
        if args.search is not None:
            search_expr = or_(
                User.last_name.ilike(f"%{args.search}%"),
                User.group.ilike(f"%{args.search}%"),
            )
            result.append(search_expr)

        return result


def get_user_teams_repository(db_session: AsyncSession = Depends(get_db_session)) -> UserTeamsRepo:
    return UserTeamsRepo(db_session)
