from typing import List, Optional

from sqlalchemy import String, asc, desc, func, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.exceptions import ObjectNotFoundException
from src.infrastructure.database.db import get_db_session
from fastapi import Depends

from src.core.domain.users.entities import UsersDTO, UsersCreateDTO, UsersListDTO
from src.infrastructure.database.models.users import User
from src.infrastructure.database.repositories.base import CRUDRepo, CreateRepo, BaseRepo


class UsersBaseRepo(BaseRepo):
    model = User

    async def get_by_credentials(self, email: str):
        select_stm = select(self.model).where(self.model.email == email)
        result = await self.session.execute(select_stm)
        obj = result.scalar()
        if obj is None:
            raise ObjectNotFoundException()
        return self.dto_model.from_orm(obj)

    async def update_by_credentials(self, email: str, **update_values):
        update_stm = update(self.model).where(self.model.email == email).values(**update_values)
        await self.session.execute(update_stm)
        await self.session.commit()
        return await self.get_by_credentials(email)


class UsersCreateRepo(UsersBaseRepo, CreateRepo):
    dto_model = UsersCreateDTO


def get_users_create_repository(db_session: AsyncSession = Depends(get_db_session)) -> UsersCreateRepo:
    return UsersCreateRepo(db_session)


class UsersRepo(UsersBaseRepo, CRUDRepo):
    dto_model = UsersDTO

    async def list(self, page: Optional[int] = None, page_size: Optional[int] = None) -> UsersListDTO:
        query = select(self.model)

        total_count = await self._get_total_count_for_query(query)

        if page_size:
            query = query.limit(page_size)
        if page_size and page:
            query = query.offset((page - 1) * page_size)

        result = await self.session.execute(query)
        users = result.scalars().all()

        return UsersListDTO(
            [UsersDTO.from_orm(user) for user in users], total_count
        )


def get_users_repository(db_session: AsyncSession = Depends(get_db_session)) -> UsersRepo:
    return UsersRepo(db_session)
