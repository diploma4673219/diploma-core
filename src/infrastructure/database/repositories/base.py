from abc import ABC
from typing import Type, List

from sqlalchemy import delete, func, insert, select, update
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Query, joinedload, selectinload

from src.core.domain.base import DTOType, ICRUDRepo, PaginationDTO, ICreateRepo
from src.core.exceptions import ObjectNotFoundException, ObjectCreateException
from src.infrastructure.database.models import Base


class BaseRepo(ABC):
    session: AsyncSession
    model: Base
    related_fields: List[str] = []
    related_collections: List[str] = []
    dto_model: Type[DTOType]

    def __init__(self, session: AsyncSession):
        self.session = session


class CreateRepo(BaseRepo, ICreateRepo[DTOType]):
    async def get_by_id(self, record_id: int) -> DTOType:
        select_stm = select(self.model).filter_by(id=record_id)

        select_stm = self._load_related_fields(select_stm)

        result = await self.session.execute(select_stm)
        obj = result.scalar()
        if obj is None:
            raise ObjectNotFoundException(f"{self.model.__name__} id={record_id} not found")
        return self.dto_model.from_orm(obj)

    def _load_related_fields(self, select_stm):
        for field in self.related_fields:
            select_stm = select_stm.options(joinedload(getattr(self.model, field)))

        for field in self.related_collections:
            select_stm = select_stm.options(selectinload(getattr(self.model, field)))

        return select_stm

    async def create(self, **creation_kwargs) -> DTOType:
        try:
            stm = insert(self.model).values(**creation_kwargs).returning(self.model)
            result = await self.session.execute(stm)
            await self.session.commit()
            new_record = result.scalar()
        except SQLAlchemyError as e:
            raise ObjectCreateException(str(e))

        return await self.get_by_id(new_record.id)


class CRUDRepo(CreateRepo, ICRUDRepo[DTOType]):
    async def get(self, **select_kwargs) -> DTOType:
        select_stm = select(self.model).filter_by(**select_kwargs)
        for field in self.related_fields:
            select_stm = select_stm.options(joinedload(getattr(self.model, field)))

        result = await self.session.execute(select_stm)
        obj = result.scalar()
        if obj is None:
            raise ObjectNotFoundException(f"{self.model.__name__} not found")
        return self.dto_model.from_orm(obj)

    async def update(self, record_id: int, **update_values) -> DTOType:
        update_stm = update(self.model).where(self.model.id == record_id).values(**update_values)
        await self.session.execute(update_stm)
        await self.session.commit()
        return await self.get_by_id(record_id)

    async def partial_update(self, record_id: int, **update_values) -> DTOType:
        update_values = {
            key: value for key, value in update_values.items() if value is not None
        }

        update_stm = update(self.model).where(self.model.id == record_id).values(**update_values)
        await self.session.execute(update_stm)
        await self.session.commit()
        return await self.get_by_id(record_id)

    async def delete(self, record_id: int) -> bool:
        stm = delete(self.model).where(self.model.id == record_id)
        result = await self.session.execute(stm)
        await self.session.commit()
        return bool(result.rowcount)

    async def list(self, *args, **kwargs) -> PaginationDTO:
        raise NotImplementedError()

    async def _get_total_count_for_query(self, query: Query) -> int:
        total_count_query = select(func.count()).select_from(query.subquery())
        total_count_result = await self.session.execute(total_count_query)
        return total_count_result.scalar()
