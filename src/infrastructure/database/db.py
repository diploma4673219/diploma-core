from asyncio import current_task
from typing import AsyncGenerator

from sqlalchemy import MetaData, exc
from sqlalchemy.ext.asyncio import AsyncSession, async_scoped_session, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy.pool import NullPool

from src.settings import settings

engine = create_async_engine(
    settings.DATABASE_URL,
    poolclass=NullPool,
)
autocommit_engine = engine.execution_options(isolation_level="AUTOCOMMIT")
SessionLocal = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
Base = declarative_base()


async def get_db_session() -> AsyncGenerator[AsyncSession, None]:
    """
    Regular, no auto-commit/transactions etc.
    """
    db: AsyncSession = async_scoped_session(
        SessionLocal,
        scopefunc=current_task,
    )()
    try:
        yield db
    except exc.DBAPIError:
        await db.rollback()
    finally:
        await db.close()


async def get_db_transactional_session() -> AsyncGenerator[AsyncSession, None]:
    session: AsyncSession = async_scoped_session(
        SessionLocal,
        scopefunc=current_task,
    )()

    try:
        yield session
    except exc.DBAPIError:
        await session.rollback()
    finally:
        await session.commit()
        await session.close()


async def get_db_autocommit_session() -> AsyncGenerator[AsyncSession, None]:
    session: AsyncSession = async_scoped_session(
        sessionmaker(
            autocommit_engine,
            expire_on_commit=False,
            class_=AsyncSession,
        ),
        scopefunc=current_task,
    )()
    try:
        yield session
    except exc.DBAPIError:
        await session.rollback()
    finally:
        await session.close()
