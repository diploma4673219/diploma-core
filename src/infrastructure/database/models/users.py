from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, String, Table, Text
from sqlalchemy.orm import relationship
from src.infrastructure.database.models import Base


class User(Base):
    __tablename__ = "users"

    id: int = Column(BigInteger, primary_key=True)
    email: str = Column(String, unique=True, nullable=False)
    password: str = Column(String, nullable=False)
    first_name: str = Column(String, nullable=True)
    last_name: str = Column(String,  nullable=True)
    middle_name: str = Column(String, nullable=True)
    group: str = Column(String, nullable=True)
    is_active: bool = Column(Boolean, default=False)
    avatar: str = Column(String, nullable=True)

    user_courses = relationship("UserCourse", back_populates="user")
    user_blocks = relationship("UserBlock", foreign_keys="[UserBlock.user_id]", back_populates="user")
    assistant_blocks = relationship("UserBlock", foreign_keys="[UserBlock.assistant_id]", back_populates="assistant")
    user_teams = relationship("UserTeam", back_populates="user")
