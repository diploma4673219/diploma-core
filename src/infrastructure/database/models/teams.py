from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from src.infrastructure.database.models import Base


class Team(Base):
    __tablename__ = "teams"

    id: int = Column(BigInteger, primary_key=True)
    course_id: int = Column(BigInteger, ForeignKey("courses.id", ondelete="CASCADE"), nullable=False)
    name: str = Column(String, nullable=False)

    course = relationship("Course", back_populates="teams")
    user_teams = relationship("UserTeam", back_populates="team")
    user_blocks = relationship("UserBlock", back_populates="team")


class UserTeam(Base):
    __tablename__ = "user_teams"

    id: int = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey("users.id", ondelete="CASCADE"), nullable=False)
    team_id = Column(BigInteger, ForeignKey("teams.id", ondelete="CASCADE"), nullable=False)
    is_leader = Column(Boolean, nullable=False)

    user = relationship("User", foreign_keys=[user_id], back_populates="user_teams")
    team = relationship("Team", back_populates="user_teams")
