from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, String, Text, Enum
from sqlalchemy.orm import relationship
from src.infrastructure.database.models import Base
from enum import IntEnum


class Course(Base):
    __tablename__ = "courses"

    id: int = Column(BigInteger, primary_key=True)
    title: str = Column(String, nullable=False)
    description: str = Column(Text, nullable=False)
    image: str = Column(String, nullable=False)
    team_settings: str = Column(String, nullable=True)

    user_courses = relationship("UserCourse", back_populates="course")
    blocks = relationship("Block", back_populates="course")
    teams = relationship("Team", back_populates="course")


class UsersRoles(IntEnum):
    MAINTAINER: int = 0
    ASSISTANT: int = 1
    STUDENT: int = 2
    ASSISTANT_REQUEST: int = 3
    STUDENT_REQUEST: int = 4
    ASSISTANT_APPROVE: int = 5


class UserCourse(Base):
    __tablename__ = "user_courses"

    id: int = Column(BigInteger, primary_key=True)
    user_id: int = Column(BigInteger, ForeignKey("users.id", ondelete="CASCADE"), nullable=False)
    course_id: int = Column(BigInteger, ForeignKey("courses.id", ondelete="CASCADE"), nullable=False)
    role: int = Column(Integer, nullable=False)

    user = relationship("User", back_populates="user_courses")
    course = relationship("Course", back_populates="user_courses")
