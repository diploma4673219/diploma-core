from sqlalchemy import BigInteger, Boolean, Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from src.infrastructure.database.models import Base


class Block(Base):
    __tablename__ = "blocks"

    id: int = Column(BigInteger, primary_key=True)
    course_id: int = Column(BigInteger, ForeignKey("courses.id", ondelete="CASCADE"), nullable=False)
    is_team: bool = Column(Boolean, default=False)
    is_project: bool = Column(Boolean, default=False)
    is_exam: bool = Column(Boolean, default=False)
    title: str = Column(String, nullable=False)
    content: str = Column(Text, nullable=False)
    file: str = Column(String, nullable=True)
    status: int = Column(Integer, default=0)
    time_hide = Column(DateTime, nullable=True)
    time_open = Column(DateTime, nullable=True)
    order: int = Column(BigInteger, nullable=True)

    course = relationship("Course", back_populates="blocks")
    user_blocks = relationship("UserBlock", back_populates="block")


class UserBlock(Base):
    __tablename__ = "user_blocks"

    id: int = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey("users.id", ondelete="CASCADE"), nullable=False)
    block_id = Column(BigInteger, ForeignKey("blocks.id", ondelete="CASCADE"), nullable=False)
    team_id = Column(BigInteger, ForeignKey("teams.id", ondelete="CASCADE"), nullable=True)
    file: str = Column(String, nullable=False)
    score: int = Column(Integer, nullable=True)
    comment: str = Column(String, nullable=True)
    assistant_id = Column(BigInteger, ForeignKey("users.id", ondelete="CASCADE"), nullable=True)
    is_author: bool = Column(Boolean, default=False)

    user = relationship("User", foreign_keys=[user_id], back_populates="user_blocks")
    block = relationship("Block", back_populates="user_blocks")
    assistant = relationship("User", foreign_keys=[assistant_id], back_populates="assistant_blocks")
    team = relationship("Team", foreign_keys=[team_id], back_populates="user_blocks")
