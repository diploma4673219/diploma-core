from enum import IntEnum, StrEnum
from dataclasses import dataclass, fields
from typing import Optional, List, Literal
from pydantic import BaseModel

from src.core.domain.base import BaseDTO, PaginationDTO, Pagination
from src.core.domain.users.entities import UsersDTO
from src.core.domain.teams.entities import TeamsDTO


class BlockStatus(IntEnum):
    HIDE = 0
    OPEN = 1
    ON_CHECK = 2
    CHECKED = 3
    ARCHIVE = 4


@dataclass
class BlocksDTO(BaseDTO):
    id: int
    title: str
    content: str
    course_id: int
    order: int
    is_team: Optional[bool] = None
    is_project: Optional[bool] = None
    is_exam: Optional[bool] = None
    file: Optional[str] = None
    status: Optional[BlockStatus] = None
    time_hide: Optional[str] = None
    time_open: Optional[str] = None


@dataclass
class BlocksCreateDTO(BaseDTO):
    title: str
    content: str
    is_team: Optional[bool] = False
    is_project: Optional[bool] = False
    is_exam: Optional[bool] = False
    file: Optional[str] = None
    time_hide: Optional[str] = None
    time_open: Optional[str] = None


@dataclass
class BlocksUpdateDTO(BaseDTO):
    title: Optional[str] = None
    content: Optional[str] = None
    is_team: Optional[bool] = None
    is_project: Optional[bool] = None
    is_exam: Optional[bool] = None
    file: Optional[str] = None
    status: Optional[BlockStatus] = None
    time_hide: Optional[str] = None
    time_open: Optional[str] = None


BlocksListDTO = List[BlocksDTO]


class BlockListFilterArgs(BaseModel):
    course_id: int
    status: List[int] = None


@dataclass
class BlockOrder(BaseDTO):
    id: int
    order: int


BlockListOrder = List[BlockOrder]


@dataclass
class UserBlocksDTO(BaseDTO):
    id: int
    user: UsersDTO
    block: BlocksDTO
    file: str
    score: Optional[int] = None
    comment: Optional[str] = None
    assistant: Optional[UsersDTO] = None
    team: Optional[TeamsDTO] = None
    team_id: Optional[int] = None

    @classmethod
    def from_orm(cls, orm_model, *args, **kwargs):
        field_names = {field.name for field in fields(cls)}
        kwargs = {name: getattr(orm_model, name) for name in field_names if hasattr(orm_model, name)}
        result = cls(**kwargs)

        result.user = UsersDTO.from_orm(result.user)
        result.block = BlocksDTO.from_orm(result.block)

        if result.assistant:
            result.assistant = UsersDTO.from_orm(result.assistant)

        if result.team:
            result.team = result.team.name

        return result


@dataclass
class UserBlocksCreateDTO(BaseDTO):
    block_id: int
    file: str


@dataclass
class UserBlocksUpdateAssistantDTO(BaseDTO):
    assistant_id: Optional[int] = None
    score: Optional[int] = None
    comment: Optional[str] = None


UserBlocksListDTO = PaginationDTO[UserBlocksDTO]


class UserBlocksListResponseDTO(BaseModel):
    result: List[UserBlocksDTO]
    pagination: Pagination


class UserBlocksListParams(BaseModel):
    search: str | None = None
    course_id: int | None = None
    block_id: int | None = None
    assistant_id: int | None = None
    score: int | None = None
    is_author: bool | None = None
    is_team: bool | None = None
    page: int = 1
    page_size: int = 10
    sort: Optional[
        Literal[
            "last_name",
            "-last_name",
            "group",
            "-group",
            "assistant",
            "-assistant",
            "score",
            "-score",
            "block",
            "-block",
            "team",
            "-team"
        ]
    ] = None


class ScorecardColumns(StrEnum):
    FULL_NAME = 'Имя и фамилия'
    GROUP = 'Группа'
    TEAM = 'Команда'
    BLOCK = 'block_title'
    SCORE = 'score'
    ORDER = 'original_index'
