import json
from collections import defaultdict
from datetime import datetime
import pytz
from fastapi import Depends, HTTPException, status, BackgroundTasks

from src.infrastructure.smtp.base import generate_scorecard_email, send_email, EMAIL_SUBJECT_SCORECARD, generate_block_finish_email, generate_assistant_finish_email
from src.infrastructure.static.user_blocks import get_csv_from_data
from .entities import BlocksDTO, UserBlocksListDTO, BlocksCreateDTO, BlocksUpdateDTO, UserBlocksDTO, \
    UserBlocksUpdateAssistantDTO, UserBlocksCreateDTO, BlocksListDTO, \
    BlockListOrder, BlockListFilterArgs, UserBlocksListParams, ScorecardColumns
from src.core.domain.courses.deps import get_current_user_course_from_id
from src.infrastructure.database.models.courses import UsersRoles
from src.infrastructure.database.repositories.blocks import BlocksRepo, get_blocks_repository, UserBlocksRepo, get_user_blocks_repository
from ..courses.entities import UserCoursesDTO, TeamSettings
from src.core.domain.permissions import verify_course_member_permission, verify_assistant_permission, AVAILABLE_BLOCK, verify_block_permission, verify_assistant_user_block_permission, verify_student_user_block_permission, verify_maintainer_permission
from src.core.exceptions import ObjectNotFoundException, HTTPException403
from src.core.domain.teams.entities import UserTeamsDTO, UserTeamsListParams
from src.core.domain.teams.deps import get_user_team, get_user_teams_repository
from src.infrastructure.database.repositories.teams import UserTeamsRepo
from src.infrastructure.database.repositories.courses import UserCoursesRepo, get_user_courses_repository

timezone = pytz.timezone('Europe/Moscow')


class BlocksService:
    def __init__(self, user_course: UserCoursesDTO, repo: BlocksRepo):
        self.user_course = user_course
        self.repo = repo

    async def get_blocks(self) -> BlocksListDTO:
        verify_course_member_permission(self.user_course.role)

        args = BlockListFilterArgs(course_id=self.user_course.course.id)

        if self.user_course.role == UsersRoles.STUDENT:
            args.status = AVAILABLE_BLOCK

        return await self.repo.list(args)

    async def get_block(self, record_id) -> BlocksDTO:
        result = await self.repo.get_by_id(record_id)

        verify_block_permission(self.user_course.role, result.status)

        return result

    async def reorder_blocks(self, data: BlockListOrder):
        verify_assistant_permission(self.user_course.role)

        try:
            for block in data:
                await self.repo.partial_update(record_id=block.id, order=block.order)
        except:
            raise HTTPException(
                status_code=status.HTTP_400_FORBIDDEN,
                detail="Ошибка изменения порядка",
            )

    async def update_block(self, record_id, updated_data: BlocksUpdateDTO) -> BlocksDTO:
        verify_assistant_permission(self.user_course.role)

        block = await self.get_block(record_id)
        if block.status != updated_data.status and self.user_course.role != UsersRoles.MAINTAINER:
            raise HTTPException403

        return await self.repo.partial_update(record_id, **updated_data.__dict__)

    async def create_block(self, created_data: BlocksCreateDTO) -> BlocksDTO:
        verify_assistant_permission(self.user_course.role)

        result = await self.repo.create(course_id=self.user_course.course.id, **created_data.__dict__)
        result = await self.repo.partial_update(record_id=result.id, order=result.id)
        return result

    async def delete_block(self, record_id) -> bool:
        verify_assistant_permission(self.user_course.role)

        return await self.repo.delete(record_id)


def get_blocks_service(
        repository: BlocksRepo = Depends(get_blocks_repository),
        user_course: UserCoursesDTO = Depends(get_current_user_course_from_id)
):
    return BlocksService(user_course, repository)


class UserBlocksService:
    def __init__(self, user_course: UserCoursesDTO,
                 repo: UserBlocksRepo,
                 blocks_repo: BlocksRepo,
                 user_teams_repo: UserTeamsRepo,
                 user_course_repo: UserCoursesRepo,
                 user_team: UserTeamsDTO | None = None):
        self.user_course = user_course
        self.repo = repo
        self.blocks_repo = blocks_repo
        self.user_teams_repo = user_teams_repo
        self.user_course_repo = user_course_repo
        self.user_team = user_team

    async def get_user_block_by_id(self, block_id):
        try:
            result = await self.repo.get(user_id=self.user_course.user.id, block_id=block_id)
        except ObjectNotFoundException:
            return None

        verify_block_permission(self.user_course.role, result.block.status)
        return result

    async def create_user_block(self, created_data: UserBlocksCreateDTO) -> UserBlocksDTO:
        block = await self.blocks_repo.get_by_id(created_data.block_id)

        verify_student_user_block_permission(self.user_course.role, block.status)

        if block.is_team:
            if self.user_team is None:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Вы не можете создать ответ к этому блоку, пока у вас нет команды",
                )

            user_teams = await self.user_teams_repo.list(UserTeamsListParams(
                course_id=self.user_course.course.id,
                team_id=self.user_team.team_id
            ))

            course_settings = TeamSettings.parse_raw(self.user_course.course.team_settings)

            if len(user_teams) < course_settings.members_amount_min or \
                    (course_settings.members_amount_max is not None and len(user_teams) > course_settings.members_amount_max):
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Вы не можете создать ответ к этому блоку, пока у вас неполная команда",
                )

            created_data.team_id = self.user_team.team_id

            try:
                check = await self.repo.get(team_id=self.user_team.team_id, block_id=created_data.block_id)
                if check is not None:
                    raise HTTPException(
                        status_code=status.HTTP_400_BAD_REQUEST,
                        detail="Ответ для этого блока уже был создан командой",
                    )
            except ObjectNotFoundException:
                pass

            result = {}
            for user_team in user_teams:
                is_author = user_team.user.id == self.user_course.user.id
                user_block = await self.repo.create(user_id=user_team.user.id, is_author=is_author,  **created_data.__dict__)

                if is_author:
                    result = user_block

            return result

        else:
            try:
                check = await self.repo.get(user_id=self.user_course.user.id, block_id=created_data.block_id)
                if check is not None:
                    raise HTTPException(
                        status_code=status.HTTP_400_BAD_REQUEST,
                        detail="Вы уже создали ответ к этому блоку",
                    )
            except ObjectNotFoundException:
                pass

            return await self.repo.create(user_id=self.user_course.user.id, is_author=True, **created_data.__dict__)

    async def delete_user_block(self, record_id) -> bool:
        user_course = await self.repo.get_by_id(record_id)

        verify_student_user_block_permission(self.user_course.role, user_course.block.status)

        return await self.repo.delete(record_id)

    async def update_user_block_student(self, record_id, updated_data: UserBlocksCreateDTO) -> UserBlocksDTO:
        block = await self.blocks_repo.get_by_id(updated_data.block_id)

        verify_student_user_block_permission(self.user_course.role, block.status)

        user_block = await self.repo.get_by_id(record_id)

        if block.is_team:
            if user_block.team_id != self.user_team.team_id:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Вы не можете обновить чужой ответ",
                )

            user_teams = await self.user_teams_repo.list(UserTeamsListParams(
                course_id=self.user_course.course.id,
                team_id=self.user_team.team_id
            ))

            result = {}
            for user_team in user_teams:
                is_author = user_team.user.id == self.user_course.user.id

                user_team_block = await self.repo.get(user_id=user_team.user.id, block_id=updated_data.block_id)
                user_team_block = await self.repo.partial_update(user_team_block.id, is_author=is_author, **updated_data.__dict__)

                if is_author:
                    result = user_team_block

            return result
        else:
            if user_block.user.id != self.user_course.user.id:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Вы не можете обновить чужой ответ",
                )

            updated_data.user_id = self.user_course.user.id

            return await self.repo.partial_update(record_id, **updated_data.__dict__)

    async def is_assistant_finish_block(self, block_id) -> bool:
        args = UserBlocksListParams(
            course_id=self.user_course.course.id,
            block_id=block_id,
            assistant_id=self.user_course.user.id,
            page=1,
            page_size=10000,
        )
        user_blocks = await self.get_user_blocks(args)

        if len(user_blocks.items) == 0:
            return False

        return all(user_block.score is not None for user_block in user_blocks.items)

    async def is_finish_block(self, block_id) -> bool:
        args = UserBlocksListParams(
            course_id=self.user_course.course.id,
            block_id=block_id,
            page=1,
            page_size=10000,
        )
        user_blocks = await self.get_user_blocks(args)

        if len(user_blocks.items) == 0:
            return False

        return all(user_block.score is not None for user_block in user_blocks.items)

    async def update_user_block_assistant(self, record_id, updated_data: UserBlocksUpdateAssistantDTO, background: BackgroundTasks) -> UserBlocksDTO:
        user_block = await self.repo.get_by_id(record_id)
        block = user_block.block
        result = {}

        verify_assistant_user_block_permission(self.user_course.role, block.status)

        if self.user_course.role != UsersRoles.MAINTAINER and \
                user_block.assistant is not None and user_block.assistant.id != self.user_course.user.id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Эту работу проверяет другой ассистент",
            )

        if block.is_team:
            if user_block.team_id is None:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                )

            user_teams = await self.user_teams_repo.list(UserTeamsListParams(
                course_id=self.user_course.course.id,
                team_id=user_block.team_id
            ))

            for user_team in user_teams:
                user_team_block = await self.repo.get(user_id=user_team.user.id, block_id=block.id)
                user_team_block = await self.repo.update(user_team_block.id, **updated_data.__dict__)

                if user_team_block.id == record_id:
                    result = user_team_block
        else:
            result = await self.repo.update(record_id, **updated_data.__dict__)

        try:
            maintainer = await self.user_course_repo.get(course_id=self.user_course.course.id, role=UsersRoles.MAINTAINER)
        except ObjectNotFoundException:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="В этом курсе нет преподавателя",
            )

        is_assistant_finish_block = await self.is_assistant_finish_block(block.id)
        if is_assistant_finish_block and self.user_course.role != UsersRoles.MAINTAINER:
            email_body = generate_assistant_finish_email(block.title, f"{self.user_course.user.last_name} {self.user_course.user.first_name}", self.user_course.user.email)
            email_subject = f"{self.user_course.course.title}. Обновление статуса проверки блока"
            background.add_task(send_email, maintainer.user.email, email_body, email_subject)

        is_finish_block = await self.is_finish_block(block.id)
        if is_finish_block:
            email_body = generate_block_finish_email(block.title)
            email_subject = f"{self.user_course.course.title}. Обновление статуса проверки блока"
            background.add_task(send_email, maintainer.user.email, email_body, email_subject)

        return result

    async def get_user_blocks(self, args: UserBlocksListParams) -> UserBlocksListDTO:
        verify_assistant_permission(self.user_course.role)

        return await self.repo.list(args)

    async def get_user_blocks_csv(self, args: UserBlocksListParams):
        verify_maintainer_permission(self.user_course.role)

        args.page = 1
        args.page_size = 100000
        user_blocks = await self.get_user_blocks(args)

        data = []
        for user_block in user_blocks.items:
            data.append({
                ScorecardColumns.FULL_NAME: f'{user_block.user.last_name} {user_block.user.first_name}',
                ScorecardColumns.GROUP: user_block.user.group,
                ScorecardColumns.TEAM: user_block.team if user_block.block.is_team else None,
                ScorecardColumns.BLOCK: user_block.block.title,
                ScorecardColumns.SCORE: user_block.score
            })

        current_datetime_with_tz = datetime.now(timezone)
        current_date_with_tz = current_datetime_with_tz.date()
        current_date_string_with_tz = current_date_with_tz.strftime('%d-%m-%Y')

        file_name = f'Ведомость_{self.user_course.course.title}_{current_date_string_with_tz}.csv'
        file_full_path = get_csv_from_data(data, file_name)

        email_body = generate_scorecard_email(file_full_path, self.user_course.course.title)
        await send_email(recipient=self.user_course.user.email, body=email_body, subject=EMAIL_SUBJECT_SCORECARD)

    async def get_user_block_statistics(self, args: UserBlocksListParams):
        verify_maintainer_permission(self.user_course.role)

        args.is_author = True
        args.page = 1
        args.page_size = 100000
        user_blocks = await self.get_user_blocks(args)

        result = defaultdict(int)
        total_checked_user_blocks = 0
        for user_block in user_blocks.items:
            score = user_block.score

            if score is not None:
                total_checked_user_blocks += 1
                result[score] += 1

        return {
            "total": user_blocks.total_count,
            "total_checked": total_checked_user_blocks,
            "scores": result
        }


def get_user_blocks_service(
        repository: UserBlocksRepo = Depends(get_user_blocks_repository),
        blocks_repository: BlocksRepo = Depends(get_blocks_repository),
        user_teams_repository: UserTeamsRepo = Depends(get_user_teams_repository),
        user_course_repository: UserCoursesRepo = Depends(get_user_courses_repository),
        user_course: UserCoursesDTO = Depends(get_current_user_course_from_id),
        user_team: UserTeamsDTO = Depends(get_user_team),
) -> UserBlocksService:
    return UserBlocksService(user_course=user_course,
                             repo=repository,
                             blocks_repo=blocks_repository,
                             user_teams_repo=user_teams_repository,
                             user_course_repo=user_course_repository,
                             user_team=user_team)
