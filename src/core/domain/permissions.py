from src.infrastructure.database.models.courses import UsersRoles
from src.core.domain.blocks.entities import BlockStatus
from ..exceptions import HTTPException403


COURSE_MEMBER_PERMISSION = [UsersRoles.STUDENT, UsersRoles.ASSISTANT, UsersRoles.MAINTAINER]
ASSISTANT_PERMISSION = [UsersRoles.ASSISTANT, UsersRoles.MAINTAINER]

AVAILABLE_BLOCK = [BlockStatus.OPEN, BlockStatus.ON_CHECK, BlockStatus.CHECKED]


def verify_course_member_permission(role: UsersRoles):
    if role not in COURSE_MEMBER_PERMISSION:
        raise HTTPException403


def verify_student_permission(role: UsersRoles):
    if not role == UsersRoles.STUDENT:
        raise HTTPException403


def verify_assistant_permission(role: UsersRoles):
    if role not in ASSISTANT_PERMISSION:
        raise HTTPException403


def verify_maintainer_permission(role: UsersRoles):
    if not role == UsersRoles.MAINTAINER:
        raise HTTPException403


def verify_block_permission(role: UsersRoles, block_status: BlockStatus):
    verify_course_member_permission(role)

    if role == UsersRoles.STUDENT and block_status not in AVAILABLE_BLOCK:
        raise HTTPException403


def verify_student_user_block_permission(role: UsersRoles, block_status: BlockStatus):
    verify_student_permission(role)
    verify_block_permission(role, block_status)

    if not block_status == BlockStatus.OPEN:
        raise HTTPException403


def verify_assistant_user_block_permission(role: UsersRoles, block_status: BlockStatus):
    verify_assistant_permission(role)

    if not block_status == BlockStatus.ON_CHECK:
        raise HTTPException403
