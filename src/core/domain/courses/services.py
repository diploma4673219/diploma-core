from fastapi import Depends, HTTPException, status

from .entities import CoursesDTO, CoursesUpdateDTO, CoursesCreateDTO, Token, UserCoursesDTO, TokenResponse, CoursesListDTO, TeamSettings
from .deps import get_current_user_course_from_token, get_current_user_course_from_id, create_course_token
from src.core.domain.users.entities import UsersDTO
from src.core.domain.users.deps import get_current_user
from src.infrastructure.database.repositories.courses import CoursesRepo, get_courses_repository, get_user_courses_repository, UserCoursesRepo
from src.infrastructure.database.models.courses import UsersRoles
from src.core.exceptions import ObjectNotFoundException
from src.core.domain.permissions import verify_assistant_permission, verify_maintainer_permission, HTTPException403


class CoursesCreateService:
    def __init__(self, user: UsersDTO, course_repo: CoursesRepo, user_course_repo: UserCoursesRepo):
        self.user = user
        self.course_repo = course_repo
        self.user_course_repo = user_course_repo

    async def create_course(self, course: CoursesCreateDTO) -> CoursesDTO:
        result = await self.course_repo.create(**course.__dict__)
        return result

    async def get_courses_list(self) -> CoursesListDTO:
        user_courses = await self.user_course_repo.list(user_id=self.user.id)
        user_courses_ids = [user_course.course.id for user_course in user_courses]

        result = await self.course_repo.list(user_courses_ids)
        return result


def get_courses_create_service(
        repository_course: CoursesRepo = Depends(get_courses_repository),
        repository_user_course: UserCoursesRepo = Depends(get_user_courses_repository),
        user: UsersDTO = Depends(get_current_user)
) -> CoursesCreateService:
    return CoursesCreateService(user=user, user_course_repo=repository_user_course, course_repo=repository_course)


class CoursesService:
    def __init__(self, user_course: UserCoursesDTO, repo: CoursesRepo):
        self.user_course = user_course
        self.repo = repo

    async def get_course(self) -> CoursesDTO:
        result = CoursesDTO.from_orm(self.user_course.course)
        result.team_settings = TeamSettings.parse_raw(result.team_settings) if (result.team_settings is not None) else None
        result.role = self.user_course.role
        return result

    async def update_course(self, course: CoursesUpdateDTO) -> CoursesDTO:
        verify_maintainer_permission(self.user_course.role)

        if course.team_settings is not None:
            if course.team_settings.members_amount_min < 2:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="В команде должно быть минимум два человека",
                )
            if course.team_settings.members_amount_max is not None and course.team_settings.members_amount_min > course.team_settings.members_amount_max:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Минимальное количество участников не может превышать максимальное",
                )

            course.team_settings = course.team_settings.json()

        result = await self.repo.partial_update(record_id=self.user_course.course.id, **course.__dict__)
        result.team_settings = TeamSettings.parse_raw(result.team_settings) if (result.team_settings is not None) else None
        result.role = self.user_course.role
        return result

    async def create_course_token_assistant(self) -> TokenResponse:
        verify_maintainer_permission(self.user_course.role)

        token = Token(course=self.user_course.course.id, role=UsersRoles.ASSISTANT_REQUEST)
        course_token = create_course_token(token)
        return TokenResponse(course_token=course_token)

    async def create_course_token_student(self) -> TokenResponse:
        verify_assistant_permission(self.user_course.role)

        token = Token(course=self.user_course.course.id, role=UsersRoles.STUDENT_REQUEST)
        course_token = create_course_token(token)
        return TokenResponse(course_token=course_token)


def get_courses_service(
        repository: CoursesRepo = Depends(get_courses_repository),
        user_course: UserCoursesDTO = Depends(get_current_user_course_from_id)
):
    return CoursesService(user_course, repository)


class UserCoursesCreateService:
    def __init__(self, user: UsersDTO, repo: UserCoursesRepo):
        self.user = user
        self.repo = repo

    async def create_user_course(self, course_token: str) -> UserCoursesDTO:
        token = get_current_user_course_from_token(course_token)

        try:
            check = await self.repo.get(user_id=self.user.id, course_id=token.course)
            if check is not None:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail="Вы уже добавлены в курс",
                )
        except ObjectNotFoundException:
            pass

        if self.user.is_active and self.user.group and self.user.first_name and self.user.last_name:
            token.role = UsersRoles.STUDENT if token.role == UsersRoles.STUDENT_REQUEST else UsersRoles.ASSISTANT

        result = await self.repo.create(user_id=self.user.id, course_id=token.course, role=token.role)
        result.user = result.user.id
        result.course = result.course.id
        return result

    async def create_user_course_maintainer(self, course_id) -> UserCoursesDTO:
        result = await self.repo.create(user_id=self.user.id, course_id=course_id, role=UsersRoles.MAINTAINER)
        result.user = result.user.id
        result.course = result.course.id
        return result


def get_user_courses_create_service(
        repository: UserCoursesRepo = Depends(get_user_courses_repository),
        user: UsersDTO = Depends(get_current_user)
) -> UserCoursesCreateService:
    return UserCoursesCreateService(user=user, repo=repository)


class UserCoursesService:
    def __init__(self, user_course: UserCoursesDTO, repo: UserCoursesRepo):
        self.user_course = user_course
        self.repo = repo

    async def update_user_course(self) -> UserCoursesDTO:
        user = self.user_course.user
        course = self.user_course.course

        if self.user_course.role not in [UsersRoles.ASSISTANT_REQUEST, UsersRoles.STUDENT_REQUEST]:
            raise HTTPException403

        if user.is_active and user.group and user.first_name and user.last_name:
            role = UsersRoles.STUDENT if self.user_course.role == UsersRoles.STUDENT_REQUEST else UsersRoles.ASSISTANT
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Вы должны заполнить все персональные данные",
            )

        result = await self.repo.update(record_id=self.user_course.id, user_id=user.id, course_id=course.id, role=role)
        result.user = result.user.id
        result.course = result.course.id
        return result


def get_user_courses_service(
        repository: UserCoursesRepo = Depends(get_user_courses_repository),
        user_course: UserCoursesDTO = Depends(get_current_user_course_from_id)
):
    return UserCoursesService(user_course, repository)
