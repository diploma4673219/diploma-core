from fastapi import HTTPException, status, Depends, Path

from src.core.domain.utils import verify_access_token, create_access_token
from src.infrastructure.database.repositories.courses import CoursesRepo, get_courses_repository, UserCoursesRepo, get_user_courses_repository
from .entities import Token, UserCoursesDTO
from src.core.domain.users.entities import UsersDTO
from src.core.domain.users.deps import get_current_user
from ...exceptions import ObjectNotFoundException


def create_course_token(token: Token) -> str:
    payload = token.json()
    return create_access_token(payload)


def get_current_user_course_from_token(course_token: str) -> Token:
    credentials_exception = HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="Ссылка-приглашение больше не действительна",
    )

    payload = verify_access_token(course_token, credentials_exception)
    token: Token = Token.parse_raw(payload)
    return token


async def get_current_user_course_from_id(
        course_id: int = Path(..., title="The ID of the item to get"),
        user: UsersDTO = Depends(get_current_user),
        courses_repo: CoursesRepo = Depends(get_courses_repository),
        user_courses_repo: UserCoursesRepo = Depends(get_user_courses_repository)
) -> UserCoursesDTO:
    credentials_exception = HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="У вас нет доступа к этому ресурсу",
    )

    try:
        course = await courses_repo.get_by_id(course_id)
        user_course = await user_courses_repo.get(user_id=user.id, course_id=course.id)
    except ObjectNotFoundException:
        raise credentials_exception

    return user_course
