from dataclasses import dataclass
from typing import Optional, List
from pydantic import BaseModel

from src.core.domain.base import BaseDTO
from src.core.domain.users.entities import UsersDTO
from src.infrastructure.database.models.courses import UsersRoles


class TeamSettings(BaseModel):
    members_amount_min: int
    members_amount_max: Optional[int]


class TokenResponse(BaseModel):
    course_token: str


class Token(BaseModel):
    course: int
    role: UsersRoles


@dataclass
class CoursesDTO(BaseDTO):
    id: int
    title: str
    description: str
    image: str
    team_settings: Optional[TeamSettings | str] = None
    role: Optional[int] = None


@dataclass
class CoursesCreateDTO(BaseDTO):
    title: str
    description: str
    image: str


@dataclass
class CoursesUpdateDTO(BaseDTO):
    title: Optional[str] = None
    description: Optional[str] = None
    image: Optional[str] = None
    team_settings: Optional[TeamSettings] = None


CoursesListDTO = List[CoursesDTO]


@dataclass
class UserCoursesDTO(BaseDTO):
    id: int
    course: CoursesDTO | int
    user: UsersDTO | int
    role: UsersRoles


UserCoursesListDTO = List[UserCoursesDTO]
