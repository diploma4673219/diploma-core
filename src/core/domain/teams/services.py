from fastapi import Depends, HTTPException, status

from .entities import TeamsDTO, TeamsCreateDTO, TeamsUpdateDTO, TeamsListDTO, TokenResponse, UserTeamsDTO, \
    UserTeamsListDTO, UserTeamsListParams
from src.infrastructure.database.repositories.teams import TeamsRepo, UserTeamsRepo
from src.core.domain.permissions import verify_course_member_permission, verify_student_permission
from ..courses.entities import UserCoursesDTO, TeamSettings
from ..utils import verify_access_token, create_access_token
from src.core.exceptions import HTTPException403


class TeamsService:
    def __init__(self, user_course: UserCoursesDTO, repo: TeamsRepo, user_team: UserTeamsDTO | None = None):
        self.user_course = user_course
        self.repo = repo
        self.user_team = user_team

    async def get_teams(self) -> TeamsListDTO:
        verify_course_member_permission(self.user_course.role)

        return await self.repo.list(course_id=self.user_course.course.id)

    async def get_team(self, record_id: int | None = None) -> TeamsDTO | None:
        verify_course_member_permission(self.user_course.role)

        if record_id is None and self.user_team is None:
            return None

        record_id = record_id if record_id else self.user_team.team_id

        return await self.repo.get_by_id(record_id)

    async def update_team(self, updated_data: TeamsUpdateDTO) -> TeamsDTO:
        verify_student_permission(self.user_course.role)

        if not self.user_team.is_leader:
            raise HTTPException403

        return await self.repo.partial_update(self.user_team.team_id, **updated_data.__dict__)

    async def create_team(self, created_data: TeamsCreateDTO) -> TeamsDTO:
        verify_student_permission(self.user_course.role)

        if self.user_team is not None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Вы уже создали команду"
            )

        return await self.repo.create(course_id=self.user_course.course.id, **created_data.__dict__)

    async def delete_team(self) -> bool:
        verify_student_permission(self.user_course.role)

        if not self.user_team.is_leader:
            raise HTTPException403

        return await self.repo.delete(self.user_team.team_id)


class UserTeamsService:
    def __init__(self, user_course: UserCoursesDTO, repo: UserTeamsRepo):
        self.user_course = user_course
        self.repo = repo

    async def get_user_team(self) -> UserTeamsDTO:
        verify_student_permission(self.user_course.role)

        args = UserTeamsListParams(course_id=self.user_course.course.id, user_id=self.user_course.user.id)
        user_teams = await self.repo.list(args)

        if len(user_teams) == 0:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Вашей команды не существует",
            )

        user_team = user_teams[0]

        return user_team

    async def is_leader(self) -> UserTeamsDTO:
        user_team = await self.get_user_team()

        if not user_team.is_leader:
            raise HTTPException403

        return user_team

    async def create_team_token(self) -> TokenResponse:
        verify_student_permission(self.user_course.role)

        team = await self.is_leader()

        user_teams = await self.repo.list(UserTeamsListParams(
            course_id=self.user_course.course.id,
            team_id=team.team_id
        ))

        course_settings = TeamSettings.parse_raw(self.user_course.course.team_settings)

        if course_settings.members_amount_max is not None and len(user_teams) == course_settings.members_amount_max:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="В вашей команде уже максимально возможное число участников"
            )

        team_token = create_access_token(team.team_id)
        return TokenResponse(team_token=team_token)

    async def create_leader(self, team_id: int) -> UserTeamsDTO:
        verify_student_permission(self.user_course.role)
        return await self.repo.create(user_id=self.user_course.user.id, team_id=team_id, is_leader=True)

    async def create_user_team(self, token: str) -> UserTeamsDTO:
        verify_student_permission(self.user_course.role)

        exception = HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Ссылка-приглашение больше не действительна",
        )

        user_teams = await self.repo.list(UserTeamsListParams(
            course_id=self.user_course.course.id,
            user_id=self.user_course.user.id
        ))

        if len(user_teams) != 0:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Вы уже добавлены в команду",
            )

        team_id = int(verify_access_token(token, exception))

        user_teams = await self.repo.list(UserTeamsListParams(
            course_id=self.user_course.course.id,
            team_id=team_id
        ))

        course_settings = TeamSettings.parse_raw(self.user_course.course.team_settings)

        if course_settings.members_amount_max is not None and len(user_teams) == course_settings.members_amount_max:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Больше нельзя добавлять участников в эту команду"
            )

        return await self.repo.create(user_id=self.user_course.user.id, team_id=team_id, is_leader=False)

    async def delete_user_team(self, record_id: int | None = None) -> bool:
        verify_student_permission(self.user_course.role)

        user_team = await self.get_user_team()

        if record_id is not None:
            if user_team.team_id != record_id and not user_team.is_leader:
                raise HTTPException403
        else:
            record_id = user_team.id

        return await self.repo.delete(record_id)

    async def get_user_teams(self, args: UserTeamsListParams) -> UserTeamsListDTO:
        verify_course_member_permission(self.user_course.role)
        return await self.repo.list(args)
