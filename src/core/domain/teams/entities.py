from dataclasses import dataclass, fields
from typing import Optional, List, Literal
from pydantic import BaseModel

from src.core.domain.base import BaseDTO
from src.core.domain.users.entities import UsersDTO


@dataclass
class TeamsCreateDTO(BaseDTO):
    name: str


@dataclass
class TeamsUpdateDTO(BaseDTO):
    name: str


class TokenResponse(BaseModel):
    team_token: str


@dataclass
class UserTeamsDTO(BaseDTO):
    id: int
    user: UsersDTO
    team_id: int
    is_leader: bool

    @classmethod
    def from_orm(cls, orm_model, *args, **kwargs):
        field_names = {field.name for field in fields(cls)}
        kwargs = {name: getattr(orm_model, name) for name in field_names if hasattr(orm_model, name)}
        result = cls(**kwargs)

        result.user = UsersDTO.from_orm(result.user)

        return result


UserTeamsListDTO = List[UserTeamsDTO]


class UserTeamsListParams(BaseModel):
    course_id: int
    user_id: int | None = None
    team_id: int | None = None
    search: str | None = None


@dataclass
class TeamsDTO(BaseDTO):
    id: int
    course_id: int
    name: str
    user_teams: List[UserTeamsDTO]

    @classmethod
    def from_orm(cls, orm_model, *args, **kwargs):
        field_names = {field.name for field in fields(cls)}
        kwargs = {name: getattr(orm_model, name) for name in field_names if hasattr(orm_model, name)}
        result = cls(**kwargs)

        if hasattr(orm_model, 'user_teams'):
            user_teams = [UserTeamsDTO.from_orm(user_team) for user_team in orm_model.user_teams]
            result.user_teams = user_teams

        return result


TeamsListDTO = List[TeamsDTO]
