from fastapi import Depends, HTTPException
from typing import Annotated

from src.core.domain.courses.deps import get_current_user_course_from_id
from ..courses.entities import UserCoursesDTO

from src.core.domain.teams.entities import UserTeamsDTO
from src.core.domain.teams.services import UserTeamsService, TeamsService
from src.infrastructure.database.repositories.teams import TeamsRepo, UserTeamsRepo, get_teams_repository, get_user_teams_repository


def get_user_teams_service(
        repository: UserTeamsRepo = Depends(get_user_teams_repository),
        user_course: UserCoursesDTO = Depends(get_current_user_course_from_id)
) -> UserTeamsService:
    return UserTeamsService(user_course=user_course, repo=repository)


async def get_user_team(
        user_team_service: Annotated[UserTeamsService, Depends(get_user_teams_service)]
) -> UserTeamsDTO | None:
    try:
        user_team = await user_team_service.get_user_team()
        return user_team
    except HTTPException:
        return None


def get_teams_service(
        repository: TeamsRepo = Depends(get_teams_repository),
        user_course: UserCoursesDTO = Depends(get_current_user_course_from_id),
        user_team: UserTeamsDTO | None = Depends(get_user_team),
):
    return TeamsService(user_course, repository, user_team)
