from abc import ABC
from dataclasses import dataclass, fields
from typing import Generic, List, Type, TypeVar
from pydantic import BaseModel

T = TypeVar("T")


class Pagination(BaseModel):
    total_count: int
    total_pages: int
    current_page: int


class PaginationDTO(Generic[T]):
    def __init__(self, items: List[T], total_count: int):
        self.items = items
        self.total_count = total_count


@dataclass
class BaseDTO:
    @classmethod
    def from_orm(cls, orm_model, *args, **kwargs):
        field_names = {field.name for field in fields(cls)}
        kwargs = {name: getattr(orm_model, name) for name in field_names if hasattr(orm_model, name)}
        return cls(**kwargs)


DTOType = TypeVar("DTOType", bound=BaseDTO)


class ICRUDRepo(Generic[DTOType], ABC):
    dto_model: Type[DTOType]

    async def list(self, *args, **kwargs) -> PaginationDTO:
        raise NotImplementedError()

    async def get(self) -> DTOType:
        raise NotImplementedError()

    async def get_by_id(self, record_id: int) -> DTOType:
        raise NotImplementedError()

    async def create(self, **creation_kwargs) -> DTOType:
        raise NotImplementedError()

    async def update(self, record_id: int, **update_values) -> DTOType:
        raise NotImplementedError()

    async def partial_update(self, record_id: int, **update_values) -> DTOType:
        raise NotImplementedError()

    async def delete(self, record_id: int) -> bool:
        raise NotImplementedError()


class ICreateRepo(Generic[DTOType], ABC):
    dto_model: Type[DTOType]

    async def get_by_id(self, record_id: int) -> DTOType:
        raise NotImplementedError()

    async def create(self, **creation_kwargs) -> DTOType:
        raise NotImplementedError()

    def _load_related_fields(self, select_stm):
        raise NotImplementedError
