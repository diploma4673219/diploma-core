from fastapi import Depends, HTTPException, status, BackgroundTasks

from .deps import get_current_user
from src.core.domain.utils import get_password_hash, verify_password, create_access_token, verify_access_token
from .entities import Token, UsersCreateDTO, UsersDTO, UsersUpdateDTO
from src.infrastructure.database.repositories.users import UsersRepo, get_users_repository, get_users_create_repository, UsersCreateRepo
from src.core.exceptions import ObjectNotFoundException, ObjectCreateException
from src.infrastructure.smtp.base import generate_confirmation_email, send_email, EMAIL_SUBJECT_CONFIRMATION


class UsersService:
    def __init__(self, user: UsersDTO, repo: UsersRepo):
        self.user = user
        self.repo = repo

    async def get_current_user(self) -> UsersDTO:
        return self.user

    async def update_current_user(self, updated_values: UsersUpdateDTO) -> UsersDTO:
        user = await self.repo.partial_update(self.user.id, **updated_values.__dict__)
        return user


def get_users_service(repository: UsersRepo = Depends(get_users_repository), user: UsersDTO = Depends(get_current_user)) -> UsersService:
    return UsersService(user=user, repo=repository)


class UsersCreateService:
    def __init__(self, repo: UsersCreateRepo):
        self.repo = repo

    async def create_token(self, email: str, password: str) -> Token:
        user = await self.get_user_by_credentials(email, password)
        access_token = create_access_token(user.email)
        return Token(access_token=access_token)

    async def get_user_by_credentials(self, email: str, password: str) -> UsersCreateDTO:
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверный адрес почты или пароль",
        )

        try:
            user = await self.repo.get_by_credentials(email)

            if not verify_password(password, user.password):
                raise credentials_exception
        except ObjectNotFoundException:
            raise credentials_exception

        return user

    async def create_user(self, email: str, password: str, background_tasks: BackgroundTasks) -> UsersCreateDTO:
        hashed_password = get_password_hash(password)

        try:
            result = await self.repo.create(email=email, password=hashed_password)
        except ObjectCreateException:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Пользователь с таким адресом почты уже существует",
            )

        email_token = create_access_token(email)
        email_body = generate_confirmation_email(email_token)
        background_tasks.add_task(send_email, email, email_body, EMAIL_SUBJECT_CONFIRMATION)

        return result

    async def verify_email(self, token: str) -> Token:
        credentials_exception = HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Пользователя не существует",
        )

        email = verify_access_token(token, credentials_exception)

        try:
            await self.repo.update_by_credentials(email, is_active=True)
        except ObjectNotFoundException:
            raise credentials_exception

        access_token = create_access_token(email)
        return Token(access_token=access_token)


def get_users_create_service(repository: UsersCreateRepo = Depends(get_users_create_repository)) -> UsersCreateService:
    return UsersCreateService(repo=repository)
