from fastapi import Depends, Request
from fastapi.security import OAuth2PasswordBearer
from typing import Optional

from src.core.domain.utils import verify_access_token
from .entities import UsersDTO
from src.infrastructure.database.repositories.users import UsersRepo, get_users_repository
from src.core.exceptions import HTTPException401


class CustomOAuth2PasswordBearer(OAuth2PasswordBearer):
    async def __call__(self, request: Request) -> Optional[str]:
        authorization: str = request.headers.get("Authorization")
        if not authorization:
            raise HTTPException401
        return await super().__call__(request)


oauth2_scheme = CustomOAuth2PasswordBearer(tokenUrl="token")


async def get_current_user(token: str = Depends(oauth2_scheme), repo: UsersRepo = Depends(get_users_repository)) -> UsersDTO:
    email = verify_access_token(token, HTTPException401())
    user = await repo.get_by_credentials(email)
    return user
