from dataclasses import dataclass
from typing import Optional
from pydantic import BaseModel

from src.core.domain.base import BaseDTO, PaginationDTO


class Token(BaseModel):
    access_token: str


class EmailToken(BaseModel):
    email_token: str


@dataclass
class UsersDTO(BaseDTO):
    id: int
    email: str
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    middle_name: Optional[str] = None
    group: Optional[int] = None
    is_active: Optional[bool] = False
    avatar: Optional[str] = None


@dataclass
class UsersUpdateDTO(BaseDTO):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    middle_name: Optional[str] = None
    group: Optional[str] = None
    avatar: Optional[str] = None


@dataclass
class UsersCreateDTO(BaseDTO):
    email: str
    password: str


UsersListDTO = PaginationDTO[UsersDTO]
