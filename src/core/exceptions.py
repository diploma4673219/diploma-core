from fastapi import HTTPException, status
from starlette.requests import Request
from starlette.responses import JSONResponse


class BaseApplicationException(Exception):
    pass


class ObjectNotFoundException(BaseApplicationException):
    pass


class ObjectCreateException(BaseApplicationException):
    pass


class ObjectUpdateException(BaseApplicationException):
    pass


class RelatedObjectNotFoundException(ObjectNotFoundException):
    pass


class HTTPException401(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Вы не авторизованы",
            headers={"WWW-Authenticate": "Bearer"},
        )


class HTTPException403(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="У вас нет доступа к этому ресурсу",
        )


async def object_error(request: Request, exc: BaseApplicationException):
    return JSONResponse({"detail": str(exc)}, status_code=400)


async def object_does_not_exist(request: Request, exc: ObjectNotFoundException):
    return JSONResponse({"detail": str(exc)}, status_code=404)


exception_handlers = {
    ObjectNotFoundException: object_does_not_exist,
    BaseApplicationException: object_error
}
