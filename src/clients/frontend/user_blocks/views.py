from math import ceil
from fastapi import APIRouter, Depends, Response, BackgroundTasks
from typing import Annotated
from pydantic import parse_obj_as

from src.core.domain.blocks.entities import UserBlocksDTO, UserBlocksUpdateAssistantDTO, UserBlocksCreateDTO, \
    UserBlocksListParams, UserBlocksListResponseDTO
from src.core.domain.blocks.services import UserBlocksService, get_user_blocks_service

router = APIRouter()


@router.post("", response_model=UserBlocksDTO)
async def create_user_block(
    form_data: UserBlocksCreateDTO,
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)]
) -> UserBlocksDTO:
    return await user_blocks_service.create_user_block(form_data)


@router.get("", response_model=UserBlocksListResponseDTO)
async def get_user_blocks(
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)],
    course_id: int,
    params: UserBlocksListParams = Depends()
) -> UserBlocksListResponseDTO:
    params.course_id = course_id
    result = await user_blocks_service.get_user_blocks(params)

    data = {
        "result": result.items,
        "pagination": {
            "total_count": result.total_count,
            "total_pages": ceil(result.total_count / params.page_size),
            "current_page": params.page,
        }
    }

    return parse_obj_as(UserBlocksListResponseDTO, data)


@router.get("/{block_id}", response_model=UserBlocksDTO | None)
async def get_user_block(
    block_id: int,
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)]
) -> UserBlocksDTO | None:
    return await user_blocks_service.get_user_block_by_id(block_id)


@router.patch("/student/{user_blocks_id}", response_model=UserBlocksDTO)
async def update_user_block_student(
    user_blocks_id: int,
    form_data: UserBlocksCreateDTO,
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)]
) -> UserBlocksDTO:
    return await user_blocks_service.update_user_block_student(user_blocks_id, form_data)


@router.patch("/assistant/{user_blocks_id}", response_model=UserBlocksDTO)
async def update_user_block_assistant(
    user_blocks_id: int,
    form_data: UserBlocksUpdateAssistantDTO,
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)],
    background_tasks: BackgroundTasks,
) -> UserBlocksDTO:
    return await user_blocks_service.update_user_block_assistant(user_blocks_id, form_data, background_tasks)


@router.delete("/{user_blocks_id}")
async def delete_user_block(
    user_blocks_id: int,
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)]
):
    await user_blocks_service.delete_user_block(user_blocks_id)
    return Response(status_code=200)


@router.get("/all/csv")
async def get_user_blocks_table(
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)],
    background_tasks: BackgroundTasks,
    course_id: int,
    params: UserBlocksListParams = Depends()
):
    params.course_id = course_id
    background_tasks.add_task(user_blocks_service.get_user_blocks_csv, params)
    return Response(status_code=200)


@router.get("/all/statistics")
async def get_user_blocks_table(
    user_blocks_service: Annotated[UserBlocksService, Depends(get_user_blocks_service)],
    course_id: int,
    params: UserBlocksListParams = Depends()
):
    params.course_id = course_id
    result = await user_blocks_service.get_user_block_statistics(params)
    return result
