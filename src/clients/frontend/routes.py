from fastapi import APIRouter
from .users.views import router as users_routes
from .courses.views import router as courses_routes
from .blocks.views import router as blocks_routes
from .user_blocks.views import router as user_blocks_routes
from .teams.views import router as teams_routes
from .files.views import router as files_routes

router = APIRouter()

router.include_router(users_routes, prefix='/users', tags=['Users'])
router.include_router(courses_routes, prefix='/courses', tags=['Courses'])
router.include_router(blocks_routes, prefix='/courses/{course_id}/blocks', tags=['Blocks'])
router.include_router(user_blocks_routes, prefix='/courses/{course_id}/user_blocks', tags=['User blocks'])
router.include_router(teams_routes, prefix='/courses/{course_id}/teams', tags=["Teams"])
router.include_router(files_routes, prefix='', tags=["Files"])
