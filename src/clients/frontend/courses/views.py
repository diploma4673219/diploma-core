from fastapi import APIRouter, Depends, Request
from typing import Annotated, List

from src.core.domain.courses.entities import CoursesDTO, CoursesUpdateDTO, CoursesCreateDTO, UserCoursesDTO, TokenResponse, CoursesListDTO
from src.core.domain.courses.services import get_courses_create_service, CoursesService, CoursesCreateService, get_courses_service, get_user_courses_create_service, UserCoursesCreateService, get_user_courses_service, UserCoursesService

router = APIRouter()


@router.post("", response_model=CoursesDTO)
async def create_course(
    form_data: CoursesCreateDTO,
    courses_service: Annotated[CoursesCreateService, Depends(get_courses_create_service)]
) -> CoursesDTO:
    course = await courses_service.create_course(form_data)
    return course


@router.get("", response_model=CoursesListDTO)
async def get_courses(
        courses_service: Annotated[CoursesCreateService, Depends(get_courses_create_service)]
) -> CoursesListDTO:
    course_list = await courses_service.get_courses_list()
    return course_list


@router.patch("/{course_id}", response_model=CoursesDTO)
async def update_course(
    form_data: CoursesUpdateDTO,
    courses_service: Annotated[CoursesService, Depends(get_courses_service)]
) -> CoursesDTO:
    course = await courses_service.update_course(form_data)
    return course


@router.get("/{course_id}", response_model=CoursesDTO)
async def get_course(
    courses_service: Annotated[CoursesService, Depends(get_courses_service)]
) -> CoursesDTO:
    course = await courses_service.get_course()
    return course


@router.get("/{course_id}/invite_student", response_model=TokenResponse)
async def update_course(
    courses_service: Annotated[CoursesService, Depends(get_courses_service)]
) -> TokenResponse:
    course_token = await courses_service.create_course_token_student()
    return course_token


@router.get("/{course_id}/invite_assistant", response_model=TokenResponse)
async def update_course(
    courses_service: Annotated[CoursesService, Depends(get_courses_service)]
) -> TokenResponse:
    course_token = await courses_service.create_course_token_assistant()
    return course_token


@router.post("/{course_id}/role", response_model=UserCoursesDTO)
async def update_role(
    user_courses_service: Annotated[UserCoursesService, Depends(get_user_courses_service)]
) -> UserCoursesDTO:
    user_course = await user_courses_service.update_user_course()
    return user_course


@router.post('/role', response_model=UserCoursesDTO)
async def create_role(
        data: TokenResponse,
        user_courses_service: Annotated[UserCoursesCreateService, Depends(get_user_courses_create_service)]
) -> UserCoursesDTO:
    user_course = await user_courses_service.create_user_course(data.course_token)
    return user_course


@router.post('/{course_id}/role/maintainer', response_model=UserCoursesDTO)
async def create_role(
        course_id: int,
        user_courses_service: Annotated[UserCoursesCreateService, Depends(get_user_courses_create_service)]
) -> UserCoursesDTO:
    user_course = await user_courses_service.create_user_course_maintainer(course_id)
    return user_course
