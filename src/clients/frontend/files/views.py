from fastapi import APIRouter, UploadFile, File
from starlette.responses import JSONResponse
from src.infrastructure.static.base import save_file

router = APIRouter()


@router.post("/upload-file")
async def upload_files(file: UploadFile = File(...)) -> JSONResponse:
    saved_file_path = save_file(
        file_content=await file.read(),
        file_name=file.filename,
    )

    response = JSONResponse({"url": saved_file_path}, status_code=200)
    return response
