from fastapi import APIRouter, Depends, BackgroundTasks
from typing import Annotated

from src.core.domain.users.entities import Token, UsersCreateDTO, UsersDTO, UsersUpdateDTO, EmailToken
from src.core.domain.users.services import get_users_service, UsersService, get_users_create_service, UsersCreateService

router = APIRouter()


@router.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: UsersCreateDTO,
    users_create_service: Annotated[UsersCreateService, Depends(get_users_create_service)],
) -> Token:
    access_token = await users_create_service.create_token(form_data.email, form_data.password)
    return access_token


@router.post("", response_model=Token)
async def create_user(
    form_data: UsersCreateDTO,
    users_create_service: Annotated[UsersCreateService, Depends(get_users_create_service)],
    background_tasks: BackgroundTasks
) -> Token:
    await users_create_service.create_user(form_data.email, form_data.password, background_tasks)
    access_token = await users_create_service.create_token(form_data.email, form_data.password)
    return access_token


@router.patch("/me", response_model=UsersDTO)
async def update_user(updated_data: UsersUpdateDTO, users_service: Annotated[UsersService, Depends(get_users_service)]):
    return await users_service.update_current_user(updated_data)


@router.get("/me", response_model=UsersDTO)
async def get_users_me(
    users_service: Annotated[UsersService, Depends(get_users_service)],
):
    return await users_service.get_current_user()


@router.post('/verify_email',  response_model=Token)
async def verify_email(
        form_data: EmailToken,
        users_create_service: Annotated[UsersCreateService, Depends(get_users_create_service)]
):
    access_token = await users_create_service.verify_email(form_data.email_token)
    return access_token
