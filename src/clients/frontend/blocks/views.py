from fastapi import APIRouter, Depends, Response
from typing import Annotated

from src.core.domain.blocks.entities import BlocksDTO, BlocksListDTO, BlocksUpdateDTO, BlocksCreateDTO, BlockListOrder
from src.core.domain.blocks.services import BlocksService, get_blocks_service

router = APIRouter()


@router.post("", response_model=BlocksDTO)
async def create_block(
    form_data: BlocksCreateDTO,
    block_service: Annotated[BlocksService, Depends(get_blocks_service)]
) -> BlocksDTO:
    return await block_service.create_block(form_data)


@router.get("", response_model=BlocksListDTO)
async def get_blocks(
    block_service: Annotated[BlocksService, Depends(get_blocks_service)]
) -> BlocksListDTO:
    return await block_service.get_blocks()


@router.get("/{block_id}", response_model=BlocksDTO)
async def get_block(
    block_id: int,
    block_service: Annotated[BlocksService, Depends(get_blocks_service)]
) -> BlocksDTO:
    return await block_service.get_block(block_id)


@router.patch("/{block_id}", response_model=BlocksDTO)
async def update_block(
    block_id: int,
    form_data: BlocksUpdateDTO,
    block_service: Annotated[BlocksService, Depends(get_blocks_service)]
) -> BlocksDTO:
    return await block_service.update_block(block_id, form_data)


@router.delete("/{block_id}")
async def delete_block(
    block_id: int,
    block_service: Annotated[BlocksService, Depends(get_blocks_service)]
):
    await block_service.delete_block(block_id)
    return Response(status_code=200)


@router.post('/bulk_reorder')
async def reorder_blocks(
    data: BlockListOrder,
    block_service: Annotated[BlocksService, Depends(get_blocks_service)]
):
    await block_service.reorder_blocks(data)
    return Response(status_code=200)
