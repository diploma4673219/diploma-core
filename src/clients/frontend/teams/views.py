from fastapi import APIRouter, Depends, Response, Path
from typing import Annotated

from src.core.domain.teams.entities import TeamsDTO, TeamsCreateDTO, TeamsUpdateDTO, UserTeamsListDTO, UserTeamsDTO, UserTeamsListParams, TeamsListDTO, TokenResponse
from src.core.domain.teams.deps import get_teams_service, TeamsService, get_user_teams_service, UserTeamsService

router = APIRouter()


@router.post("", response_model=TeamsDTO)
async def create_team(
    form_data: TeamsCreateDTO,
    team_service: Annotated[TeamsService, Depends(get_teams_service)],
    user_team_service: Annotated[UserTeamsService, Depends(get_user_teams_service)],
) -> TeamsDTO:
    result = await team_service.create_team(form_data)
    await user_team_service.create_leader(result.id)
    result = await team_service.get_team(result.id)
    return result


@router.get("", response_model=TeamsListDTO)
async def get_teams(
) -> TeamsListDTO:
    pass


@router.get("/{team_id}", response_model=TeamsDTO | None)
async def get_team(
    team_id: int,
    team_service: Annotated[TeamsService, Depends(get_teams_service)],
) -> TeamsDTO | None:
    team_id = None if team_id == -1 else team_id
    return await team_service.get_team(team_id)


@router.patch("", response_model=TeamsDTO)
async def update_team(
    form_data: TeamsUpdateDTO,
    team_service: Annotated[TeamsService, Depends(get_teams_service)],
) -> TeamsDTO:
    return await team_service.update_team(form_data)


@router.delete("/{team_id}")
async def delete_team(
    team_id: int,
    team_service: Annotated[TeamsService, Depends(get_teams_service)],
):
    await team_service.delete_team()
    return Response(status_code=200)


@router.get("/member/invite", response_model=TokenResponse)
async def invite_member(
    user_team_service: Annotated[UserTeamsService, Depends(get_user_teams_service)],
) -> TokenResponse:
    return await user_team_service.create_team_token()


@router.post("/member", response_model=UserTeamsDTO)
async def create_member(
    data: TokenResponse,
    user_team_service: Annotated[UserTeamsService, Depends(get_user_teams_service)],
) -> UserTeamsDTO:
    return await user_team_service.create_user_team(data.team_token)


@router.delete("/member/{user_id}", response_model=TokenResponse)
async def delete_member(
    user_id: int,
    user_team_service: Annotated[UserTeamsService, Depends(get_user_teams_service)],
):
    user_id = None if user_id == -1 else user_id
    await user_team_service.delete_user_team(user_id)
    return Response(status_code=200)
