import os

SECRET_KEY = "ff9512ef4d2759a05acec5155e833c2c703b0d80871a0ab1b52d98df1b128477"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 60 * 24 * 7

origins = ["*"]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


class Settings:
    HOST: str = os.getenv("HOST", "0.0.0.0")
    PORT: int = int(os.getenv("PORT", 8000))
    EXTERNAL_HOST: str = os.getenv("EXTERNAL_HOST", "http://127.0.0.1:8000")

    DATABASE_NAME: str = os.getenv("DATABASE_NAME", "diploma-core")
    DATABASE_USER: str = os.getenv("DATABASE_USER", "diploma-core")
    DATABASE_PASSWORD: str = os.getenv("DATABASE_PASSWORD", "diploma-core")
    DATABASE_HOST: str = os.getenv("DATABASE_HOST", "0.0.0.0")
    DATABASE_PORT: int = os.getenv("DATABASE_PORT", 5432)

    SMTP_SERVER = "smtp.gmail.com"
    SMTP_PORT = 587
    SMTP_USERNAME = "zubarenik@gmail.com"
    SMTP_PASSWORD = "zlonhtsxukljhgjx"

    FRONTEND_URL = os.getenv("FRONTEND_URL", "http://localhost:8081")

    @property
    def DATABASE_URL(self) -> str:
        return f"postgresql+asyncpg://{self.DATABASE_USER}:{self.DATABASE_PASSWORD}@{self.DATABASE_HOST}:{self.DATABASE_PORT}/{self.DATABASE_NAME}"


settings = Settings()
