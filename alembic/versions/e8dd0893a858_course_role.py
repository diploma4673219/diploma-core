"""course role

Revision ID: e8dd0893a858
Revises: 619ababa9cf2
Create Date: 2024-05-14 03:06:39.953951

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision: str = 'e8dd0893a858'
down_revision: Union[str, None] = '619ababa9cf2'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user_courses', 'role')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user_courses', sa.Column('role', postgresql.ENUM('MAINTAINER', 'ASSISTANT', 'STUDENT', name='userroles'), autoincrement=False, nullable=False))
    # ### end Alembic commands ###
